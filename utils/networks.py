import os
import json
import torch
import numpy as np
import networkx as nx
import utils.graphNetHelpers as gh


class GraphNeuralNet(torch.nn.Module):

    def __init__(self,
                 name='physics_induced_bias_net',
                 edge_input_dim=2,
                 node_input_dim=2,
                 global_input_dim=2,
                 node_enc_dim=50,
                 edge_enc_dim=50,
                 global_enc_dim=50,
                 num_gn_block=3,
                 normalization=[1, 2, 2, 2],
                 num_neurons=[256, 256, 128],
                 drop_probability=0.0,
                 attention=[False, False, True],
                 residual=True,
                 device=None,
                 output_activation='ReLU',
                 gn_output_activation='LeakyReLU',
                 hidden_activation='LeakyReLU',
                 attention_out_activation='sigmoid',
                 physics_induced_bias=[False, False, True],
                 use_speed=True):

        super(GraphNeuralNet, self).__init__()

        # check parameter integrity

        if not num_gn_block == len(normalization)-1:

            # At the first layer of each MLP in GN blocks, some normalization will be added based on
            # normalization parameters.
            # For instance, when num_gn_block=3 and normalization=[1,2,2,1],
            # At the first layer of each MLP in the first GN block, Batch norm will be applied.
            # At the first layer of each MLP in the second GN block, Layer norm will be applied.
            # At the first layer of each MLP in the third GN block, Layer norm will be applied.
            # At the first layer of each MLP in the GP block, Batch norm will be applied.

            raise RuntimeError("Number of GN blocks is not compatible with the given normalization methods list")

        if not num_gn_block == len(attention):
            raise RuntimeError("Number of GN blocks is not compatible with the given attention list")

        # Set attributes for save/load procedures
        self.save_param_dict = dict()
        self._set_param('name', name)
        self._set_param('edge_input_dim', edge_input_dim)
        self._set_param('node_input_dim', node_input_dim)
        self._set_param('global_input_dim', global_input_dim)
        self._set_param('node_enc_dim', node_enc_dim)
        self._set_param('edge_enc_dim', edge_enc_dim)
        self._set_param('global_enc_dim', global_enc_dim)
        self._set_param('num_gn_block', num_gn_block)
        self._set_param('num_neurons', num_neurons)
        self._set_param('residual', residual)
        self._set_param('output_activation', output_activation)
        self._set_param('normalization', normalization)
        self._set_param('attention', attention)
        self._set_param('use_speed', use_speed)
        self._set_param('hidden_activation', hidden_activation)
        self._set_param('gn_output_activation', gn_output_activation)
        self._set_param('drop_probability', drop_probability)
        self._set_param('attention_out_activation', attention_out_activation)
        self._set_param('physics_induced_bias', physics_induced_bias)

        if device is None:
            self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        else:
            self.device = device

        self.layers = torch.nn.ModuleList()
        input_gn_block = gh.GraphNetworkBlock(edge_input_dim=edge_input_dim,
                                              node_input_dim=node_input_dim,
                                              global_input_dim=global_input_dim,
                                              edge_encoding_dim=edge_enc_dim,
                                              node_encoding_dim=node_enc_dim,
                                              global_encoding_dim=global_enc_dim,
                                              normalization=self.normalization[0],
                                              attention=self.attention[0],
                                              num_neurons=self.num_neurons,
                                              residual=False,
                                              device=self.device,
                                              hidden_activation=self.hidden_activation,
                                              out_activation=self.gn_output_activation,
                                              attention_out_activation=self.attention_out_activation,
                                              physics_induced_bias=self.physics_induced_bias[0],
                                              drop_probability=drop_probability)
        input_gn_block.to(device)
        self.layers.append(input_gn_block)
        for i in range(num_gn_block-1):
            gn_block = gh.GraphNetworkBlock(edge_input_dim=edge_enc_dim,
                                            node_input_dim=node_enc_dim,
                                            global_input_dim=global_enc_dim,
                                            edge_encoding_dim=edge_enc_dim,
                                            node_encoding_dim=node_enc_dim,
                                            global_encoding_dim=global_enc_dim,
                                            normalization=self.normalization[i+1],
                                            attention=self.attention[i+1],
                                            num_neurons=self.num_neurons,
                                            residual=self.residual,
                                            device=self.device,
                                            hidden_activation=self.hidden_activation,
                                            out_activation=self.gn_output_activation,
                                            attention_out_activation=self.attention_out_activation,
                                            physics_induced_bias=self.physics_induced_bias[i+1],
                                            drop_probability=drop_probability)
            gn_block.to(device)
            self.layers.append(gn_block)

        gp_block = gh.GraphPredBlock(node_input_dim=node_enc_dim,
                                     num_neurons=num_neurons,
                                     out_activation=self.output_activation,
                                     hidden_activation=self.hidden_activation,
                                     normalization=self.normalization[-1],
                                     drop_probability=drop_probability,
                                     device=self.device)

        gp_block.to(device)
        self.layers.append(gp_block)

        # Training related attributes

        self.global_train_step = 0
        self.optimizer = torch.optim.Adam(self.parameters())
        self.lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(self.optimizer, verbose=True)

    def _set_param(self, p_name, p_value):
        setattr(self, p_name, p_value)
        self.save_param_dict[p_name] = p_value

    def forward(self, graphs):
        ret = [gh.graph2inputs(graph, self.use_speed, self.device) for graph in graphs]  # pytoch-variabled graphs
        for layer in self.layers:
            ret = layer(ret)
        return ret

    def get_attention(self, graphs, layer_index=None):
        if layer_index is None:
            layer_index = self.num_gn_block-1

        ret = [gh.graph2inputs(graph, self.use_speed, self.device) for graph in graphs]
        if layer_index == 0:
            ret = self.layers[0].get_attention(ret)
        else:
            for layer in self.layers[:layer_index+1]:
                ret = layer(ret)

            ret = self.layers[layer_index].get_attention(ret)
        return ret

    def predict(self, graphs, return_cutoff=True):
        with torch.no_grad():
            y_preds = self(graphs)

        out_gs = [nx.DiGraph() for _ in y_preds]

        for pred_g, out_g in zip(y_preds, out_gs):
            for n in pred_g.nodes:
                pred = pred_g.nodes[n]['tensor'].cpu().numpy()
                if return_cutoff and pred >= 1.0:
                    pred = 1.0
                out_g.add_node(n, power=pred)
        return out_gs

    def _test(self, x_graphs, y_graphs, loss='RMSE'):
        y_preds, y_targets = self._prepare_inputs(x_graphs, y_graphs)
        if loss == 'RMSE':
            l2_loss = torch.pow((y_preds-y_targets), 2).mean()
            loss = torch.sqrt(l2_loss)
        elif loss == 'MAPE':
            loss = 100 * torch.abs((y_targets - y_preds) / y_targets).mean()
        else:
            raise RunTimeError("Not valid test mode")
        return loss

    def test(self, x_graphs, y_graphs, loss='RMSE'):
        with torch.no_grad():
            loss = self._test(x_graphs, y_graphs, loss).cpu().numpy()
        return loss

    @staticmethod
    def softmax_weighed_loss(temperature):
        def _softmax_weighed_loss(outputs, targets):
            l2_loss = torch.pow((outputs-targets), 2)
            softmax_numer = torch.exp(l2_loss / temperature)
            softmax_denom = torch.sum(torch.exp(l2_loss / temperature), dim=0)
            softmax_weight = softmax_numer / softmax_denom
            batch_size = outputs.size()[0]
            adjusted_softmax_weight = torch.mul(softmax_weight, batch_size)
            losses = adjusted_softmax_weight * l2_loss
            weighted_mse = losses.mean()
            return weighted_mse

        return _softmax_weighed_loss

    @staticmethod
    def annealed_temperature(step, init_temp, eps=1e-5):
        # Add 1 to make return temperature to be lager than 1.0
        at = init_temp * np.log(step + np.exp(1) + eps)
        at = init_temp * (step + np.exp(1) + eps)
        if at >= 100.0:
            at = 100.0
        return at

    @staticmethod
    def mse_loss():
        return torch.nn.MSELoss()

    def _prepare_inputs(self, x_graphs, y_graphs):
        y_pred_graphs = self(x_graphs)
        y_preds = []
        y_targets = []
        for y_pred_g, y_target_g in zip(y_pred_graphs, y_graphs):
            for pred_n, _ in zip(y_pred_g.nodes, y_target_g.nodes):
                y_preds.append(y_pred_g.nodes[pred_n]['tensor'])
                y_n_power = y_target_g.nodes[pred_n]['power']
                y_n_power = torch.tensor(y_n_power).float().to(self.device)
                y_targets.append(y_n_power)

        b_y_preds = torch.stack(y_preds, dim=0)
        b_y_targets = torch.unsqueeze(torch.stack(y_targets, dim=0), dim=-1)
        return b_y_preds, b_y_targets

    def fit(self, x_graphs, y_graphs,
            val_x_graphs=None, val_y_graphs=None,
            train_steps=500, show_steps=100, val_steps=1000, temperature=3.0, temp_threshold=100, use_mse=True):

        hist = dict()
        hist['steps'] = []
        hist['train_loss'] = []
        hist['val_loss'] = []

        loss_switched = False

        for _train_steps in range(train_steps):
            # Assume the first dimension is designated for batch
            b_y_preds, b_y_targets = self._prepare_inputs(x_graphs, y_graphs)

            if use_mse:
                loss = self.mse_loss()(b_y_preds, b_y_targets)
            else:
                annealed_temp = self.annealed_temperature(_train_steps, temperature)
                if annealed_temp >= temp_threshold:  # when temperature is high_enough
                    if not loss_switched:
                        print("loss temp is now plain MSE ")
                        loss_switched = True
                    loss = self.mse_loss()(b_y_preds, b_y_targets)
                else:
                    loss = self.softmax_weighed_loss(annealed_temp)(b_y_preds, b_y_targets)

            # Back-prop and Optimize
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()

            # Compute validation loss if necessary
            if val_x_graphs is None or val_y_graphs is None:
                # Inform training steps
                if _train_steps % show_steps == 0:
                    print("[{}/{}]".format(_train_steps, train_steps))
            else:
                val_loss = self._test(val_x_graphs, val_y_graphs)
                self.lr_scheduler.step(val_loss)
                if self.global_train_step % val_steps == 0:
                    np_train_loss = loss.detach().cpu().numpy()
                    np_val_loss = val_loss.detach().cpu().numpy()
                    print("[{}/{}]".format(_train_steps, train_steps))
                    if not use_mse:
                        print("annealed_temp : {}".format(annealed_temp))
                    print("train loss : {}".format(np_train_loss))
                    print("validation loss : {}".format(np_val_loss))

                    hist['steps'].append(_train_steps)
                    hist['train_loss'].append(np_train_loss.tolist())
                    hist['val_loss'].append(np_val_loss.tolist())

            self.global_train_step += 1  # global_step update

        # reset optimizer and scheduler
        self.global_train_step = 0
        self.optimizer = torch.optim.Adam(self.parameters())
        self.lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(self.optimizer, verbose=True)

        return hist

    def save(self, save_path):
        """
        :param save_path: full path for saving model which include file names
            ex) save_path = './model_saves/model_name'
        """

        model_name = os.path.split(save_path)[-1]

        # save pytorch model
        torch.save(self.state_dict(), save_path)

        # save model params and pytorch model path
        j = self.save_param_dict
        j['save_path'] = model_name
        json_path = save_path + '.json'
        with open(json_path, 'w') as f:
            json.dump(j, f)

    @classmethod
    def load(cls, load_path, device=None):

        """
        :param load_path: full path for loading model which include file names
            ex) load_path = './model_saves/model_name'
        """

        json_load_path = load_path + '.json'
        with open(json_load_path) as f:
            j = json.load(f)
            j.pop('save_path')
            if device is not None:
                j['device'] = device

        gnn_load = cls(**j)
        gnn_load.load_state_dict(torch.load(load_path))
        return gnn_load
