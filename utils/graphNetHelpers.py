import torch
from torch.nn.parameter import Parameter
import numpy as np
import math


def aggregate_messages(messages, method='mean', aggregation_axis=0):
    """
    :param messages: Message tensor w/ dimension of [# Messages, Message dims]
    :param method: A string ['mean','max'] or a callable function that return tensorflow operation
    :param aggregation_axis: (int) aggregation axis
    :return: Aggregated message w/ deimension of [Batch size, Message dims]
    """

    if method == 'mean':
        aggregated_message = torch.mean(messages, dim=aggregation_axis)
    elif method == 'max':
        aggregated_message = torch.max(messages, dim=aggregation_axis)
    else:
        aggregated_message = method(messages)
    return aggregated_message


def get_node_attr_keys(use_speed=False):
    """ A helper function for getting dictionary keys in persistent way
    :return: list of 'keys'
    """
    keys = ['dummy']
    if use_speed:
        keys += ['speed']
    return keys


def get_edge_attr_keys():
    """ A helper function for getting dictionary keys in persistent way
    :return: list of 'keys'
    """
    keys = ['x', 'r']
    return keys


def get_global_attr_keys():
    keys = ['wind_direction', 'wind_speed']
    return keys


def graph2inputs(graph, use_speed=True, device=None):

    if device is None:
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    else:
        device = device

    g = graph.copy()
    # Processing node attributes
    for n in graph.nodes:
        node_attrs = graph.nodes[n]
        keys = get_node_attr_keys(use_speed)
        __input = np.concatenate([np.array(node_attrs[k], dtype=float).flatten() for k in keys]).reshape(-1)
        __input = torch.tensor(__input).float().to(device)
        g.add_node(n, tensor=__input)

    # Processing edge attributes
    for e in graph.edges:
        sender = e[0]
        receiver = e[1]
        edge_attrs = graph.edges[e]
        keys = get_edge_attr_keys()
        __input = np.concatenate([np.array(edge_attrs[k], dtype=float).flatten() for k in keys]).reshape(-1)
        __input = torch.tensor(__input).float().to(device)
        g.add_edge(sender, receiver, tensor=__input)

    # Processing global attributes
    keys = get_global_attr_keys()
    __input = np.concatenate([np.array(graph.graph[k], dtype=float).flatten() for k in keys]).reshape(-1)
    __input = torch.tensor(__input).float().to(device)
    g.graph['tensor'] = __input

    return g


class MultiLayerPerceptron(torch.nn.Module):

    def __init__(self,
                 input_dimension,
                 output_dimension,
                 num_neurons=[128, 128, 128],
                 input_normalization=2,
                 hidden_activation='SELU',
                 out_activation=None,
                 drop_probability=0.5):
        """
        :param num_neurons: number of neurons for each layer
        :param out_activation: output layer's activation unit
        :param input_norm: input normalization behavior flag
        0: Do not normalize, 1: Batch normalization, 2: Layer normalization
        :param hidden_activation: hidden layer activation units. supports 'ReLU','SELU','LeakyReLU','sigmoid'
        """

        super().__init__()
        self.layers = torch.nn.ModuleList()
        self.input_dimension = input_dimension
        self.output_dimension = output_dimension
        self.out_activation = out_activation
        self.input_normalization = input_normalization
        self.hidden_activation = hidden_activation
        self.drop_probability = drop_probability

        # infer normalization layers
        if self.input_normalization == 0:
            pass
        else:
            if self.input_normalization == 1:
                norm_layer = torch.nn.BatchNorm1d(self.input_dimension)
            elif self.input_normalization == 2:
                norm_layer = torch.nn.LayerNorm(self.input_dimension)
            self.layers.append(norm_layer)

        self.layers.append(torch.nn.Linear(self.input_dimension, num_neurons[0]))  # input -> hidden 1
        for i, num_neuron in enumerate(num_neurons[:-1]):
            self.layers.append(torch.nn.Linear(num_neuron, num_neurons[i+1]))
        self.layers.append(torch.nn.Linear(num_neurons[-1], self.output_dimension))  # hidden_n -> output

    def forward(self, x):
        if self.input_normalization != 0:  # The first layer is not normalization layer
            out = self.layers[0](x)
            for layer in self.layers[1:-1]:  # Linear layer starts from layers[1]
                out = layer(out)
                if self.drop_probability > 0.0:
                    out = self.infer_dropout(self.drop_probability)(out)  # Apply dropout
                out = self.infer_activation(self.hidden_activation)(out)

            out = self.layers[-1](out)  # The last linear layer
            if self.out_activation is None:
                pass
            else:
                out = self.infer_activation(self.out_activation)(out)
        else:
            out = x
            for layer in self.layers[:-1]:
                out = layer(out)
                if self.drop_probability > 0.0:
                    out = self.infer_dropout(self.drop_probability)(out)
                out = self.infer_activation(self.hidden_activation)(out)

            out = self.layers[-1](out)
            # infer output activation units
            if self.out_activation is None:
                pass
            else:
                out = self.infer_activation(self.out_activation)(out)
        return out

    def infer_activation(self, activation):
        if activation == 'ReLU':
            ret = torch.nn.ReLU()
        elif activation == 'sigmoid':
            ret = torch.nn.Sigmoid()
        elif activation == 'SELU':
            ret = torch.nn.SELU()
        elif activation == 'LeakyReLU':
            ret = torch.nn.LeakyReLU()
        elif activation == 'PReLU':
            ret = torch.nn.PReLU()
        elif activation == 'GELU':
            ret = GELU()
        else:
            raise RuntimeError("Given {} activation is not supported".format(self.out_activation))
        return ret

    @staticmethod
    def infer_dropout(p):
        if p >= 0.0:
            ret = torch.nn.Dropout(p=p)
        return ret


class GELU(torch.nn.Module):

    def forward(self, x):
        return 0.5 * x * (1 + torch.tanh(math.sqrt(2 / math.pi) * (x + 0.044715 * torch.pow(x, 3))))


class GraphNetworkBlock(torch.nn.Module):

    def __init__(self,
                 edge_input_dim,
                 node_input_dim,
                 global_input_dim,
                 device,
                 edge_encoding_dim=30,
                 node_encoding_dim=30,
                 global_encoding_dim=30,
                 normalization=0,
                 attention=True,
                 num_neurons=[128, 128],
                 drop_probability=0.5,
                 residual=True,
                 out_activation='ReLU',
                 hidden_activation='ReLU',
                 attention_out_activation='sigmoid',
                 physics_induced_bias=False):

        super(GraphNetworkBlock, self).__init__()

        self.device = device
        self.num_neurons = num_neurons
        self.out_activation = out_activation
        self.hidden_activation = hidden_activation
        self.attention_out_activation = attention_out_activation
        self.residual = residual
        self.edge_enc_dim = edge_encoding_dim
        self.node_enc_dim = node_encoding_dim
        self.global_enc_dim = global_encoding_dim
        self.normalization = normalization
        self.attention = attention
        self.drop_probability = drop_probability
        self.physics_induced_bias = physics_induced_bias

        edge_enc_input_dim = edge_input_dim+2*node_input_dim+global_input_dim
        node_enc_input_dim = edge_encoding_dim+node_input_dim+global_input_dim
        global_enc_input_dim = edge_encoding_dim+node_encoding_dim+global_input_dim

        edge_encoder_dict = self._prepare_mlp_params(edge_enc_input_dim, edge_encoding_dim)
        node_encoder_dict = self._prepare_mlp_params(node_enc_input_dim, node_encoding_dim)
        global_encoder_dict = self._prepare_mlp_params(global_enc_input_dim, global_encoding_dim)

        edge_enc = MultiLayerPerceptron(**edge_encoder_dict).to(device)
        node_enc = MultiLayerPerceptron(**node_encoder_dict).to(device)
        global_enc = MultiLayerPerceptron(**global_encoder_dict).to(device)
        module_list = [edge_enc, node_enc, global_enc]

        if self.attention:  # Instantiate a network for computing attention weights for node update
            if not self.physics_induced_bias:
                attention_encoder_dict = self._prepare_mlp_params(3, 1)
                attention_encoder_dict['out_activation'] = self.attention_out_activation
                attention_encoder_dict['input_normalization'] = 0  # No normalization will be added for weight computing
                attention_enc = MultiLayerPerceptron(**attention_encoder_dict).to(device)
            else:
                attention_enc = PhysicsInducedBias()
            module_list += [attention_enc]

        self.encoders = torch.nn.ModuleList(module_list)

    def _prepare_mlp_params(self, input_dim, output_dim):
        ret_dict = dict()
        ret_dict['input_dimension'] = input_dim
        ret_dict['output_dimension'] = output_dim
        ret_dict['num_neurons'] = self.num_neurons
        ret_dict['hidden_activation'] = self.hidden_activation
        ret_dict['out_activation'] = self.out_activation
        ret_dict['input_normalization'] = self.normalization
        ret_dict['drop_probability'] = self.drop_probability
        return ret_dict

    def get_attention(self, graphs):
        if not self.attention:
            raise RuntimeError("This layer do not implement attention mechanism")

        gs = [g.copy() for g in graphs]
        num_edges = [len(graph.edges) for graph in graphs]

        # Edge update routine
        concat_edge_attrs = []
        attention_attrs = []
        for graph in graphs:
            global_attr = graph.graph['tensor']
            for e in graph.edges:
                edge_attr = graph.edges[e]['tensor']
                sender_attr = graph.nodes[e[0]]['tensor']  # Sender node attr
                receiver_attr = graph.nodes[e[1]]['tensor']  # Receiver node attr

                x = graph.edges[e]['x']
                r = graph.edges[e]['r']
                global_speed = graph.graph['wind_speed']
                __exo = torch.Tensor(np.array((x, r, global_speed))).to(self.device).reshape(-1)
                concat_attr = torch.cat((edge_attr, sender_attr, receiver_attr, global_attr), dim=0)

                concat_edge_attrs.append(concat_attr)
                attention_attrs.append(__exo)

        b_edge_enc_input = torch.stack(concat_edge_attrs, dim=0)
        b_attn_enc_input = torch.stack(attention_attrs, dim=0)
        b_encoded_edge = self.encoders[0](b_edge_enc_input)
        b_attention = self.encoders[3](b_attn_enc_input)

        b_encoded_edge_split = torch.split(b_encoded_edge, split_size_or_sections=num_edges)
        b_attention_split = torch.split(b_attention, split_size_or_sections=num_edges)

        for i, (g, encoded_edges, attentions) in enumerate(zip(gs, b_encoded_edge_split, b_attention_split)):
            _iter = zip(graphs[i].edges, torch.unbind(encoded_edges, dim=0), torch.unbind(attentions, dim=0))
            for e, edge_encoded, attention in _iter:
                g.edges[(e[0], e[1])]['tensor'] = edge_encoded
                g.edges[(e[0], e[1])]['attention'] = attention

        return gs

    def forward(self, graphs):
        gs = [g.copy() for g in graphs]  # Will be updated graphs
        num_edges = [len(graph.edges) for graph in graphs]
        num_nodes = [len(graph.nodes) for graph in graphs]
        num_globals = [1 for _ in graphs]

        # Edge update routine
        concat_edge_attrs = []
        for graph in graphs:
            global_attr = graph.graph['tensor']
            for e in graph.edges:
                edge_attr = graph.edges[e]['tensor']
                sender_attr = graph.nodes[e[0]]['tensor']  # Sender node attr
                receiver_attr = graph.nodes[e[1]]['tensor']  # Receiver node attr
                concat_attr = torch.cat((edge_attr, sender_attr, receiver_attr, global_attr), dim=0)

                concat_edge_attrs.append(concat_attr)

        b_edge_enc_input = torch.stack(concat_edge_attrs, dim=0)
        b_encoded_edge = self.encoders[0](b_edge_enc_input)
        b_encoded_edge_split = torch.split(b_encoded_edge, split_size_or_sections=num_edges)

        for i, (g, encoded_edges) in enumerate(zip(gs, b_encoded_edge_split)):
            for e, edge_encoded in zip(graphs[i].edges, torch.unbind(encoded_edges, dim=0)):
                g.edges[(e[0], e[1])]['tensor'] = edge_encoded

        # Node update routine
        concat_node_attrs = []
        for i, graph in enumerate(graphs):
            for n in graph.nodes:
                messages = []
                exos = []
                if gs[i].in_degree[n] != 0:
                    # Collecting all incoming updated edges
                    for e in gs[i].in_edges(n):
                        x = gs[i].edges[e]['x']
                        r = gs[i].edges[e]['r']
                        global_speed = graph.graph['wind_speed']
                        __exo = torch.Tensor(np.array((x, r, global_speed))).to(self.device).reshape(-1)
                        messages.append(gs[i].edges[e]['tensor'])
                        exos.append(__exo)
                else:  # has no incoming edges
                    messages = [torch.zeros(size=(self.edge_enc_dim,)).to(self.device)]
                    exos = [torch.zeros(size=(3,)).to(self.device)]

                messages = torch.stack(messages, dim=0)
                exos = torch.stack(exos, dim=0)

                if self.attention:
                    attentions = self.encoders[3](exos)  # [Num input messages X 1]
                    messages = attentions * messages

                aggregated_message = aggregate_messages(messages)
                node_attr = graph.nodes[n]['tensor']
                global_attr = graph.graph['tensor']
                concat_attr = torch.cat((aggregated_message, node_attr, global_attr), dim=0)
                concat_node_attrs.append(concat_attr)

        b_node_enc_input = torch.stack(concat_node_attrs, dim=0)
        b_encoded_node = self.encoders[1](b_node_enc_input)
        b_encoded_node_split = torch.split(b_encoded_node, split_size_or_sections=num_nodes)

        for i, (g, encode_nodes) in enumerate(zip(gs, b_encoded_node_split)):
            for n, node_encoded in zip(graphs[i].nodes, torch.unbind(encode_nodes, dim=0)):
                g.node[n]['tensor'] = node_encoded

        # Global update routine
        concat_global_attrs = []
        for i, g in enumerate(gs):
            updated_node_attrs = [g.nodes[n]['tensor'] for n in g.nodes]
            updated_node_attrs = torch.stack(updated_node_attrs, dim=0)
            aggregated_node_attr = aggregate_messages(updated_node_attrs)

            updated_edge_attrs = [g.edges[e]['tensor'] for e in g.edges]
            updated_edge_attrs = torch.stack(updated_edge_attrs, dim=0)
            aggregated_edge_attr = aggregate_messages(updated_edge_attrs)

            concat_global_attr = torch.cat((aggregated_edge_attr,
                                            aggregated_node_attr,
                                            graphs[i].graph['tensor']), dim=0)
            concat_global_attrs.append(concat_global_attr)

        b_g_enc_input = torch.stack(concat_global_attrs, dim=0)
        b_g_encoded = self.encoders[2](b_g_enc_input)
        b_encoded_g_split = torch.split(b_g_encoded, split_size_or_sections=num_globals)

        for g, encode_global in zip(gs, b_encoded_g_split):
            g.graph['tensor'] = torch.squeeze(encode_global)

        if self.residual:
            for g, graph in zip(gs, graphs):

                # add residual connection to node attributes
                for n in graph.nodes:
                    g.nodes[n]['tensor'] = g.nodes[n]['tensor'] + graph.nodes[n]['tensor']

                # add residual connection to edge attributes
                for e in graph.edges:
                    g.edges[e]['tensor'] = g.edges[e]['tensor'] + graph.edges[e]['tensor']

                # add residual connection to global attribute
                g.graph['tensor'] += graph.graph['tensor']

        return gs


class GraphPredBlock(torch.nn.Module):

    def __init__(self,
                 node_input_dim,
                 device,
                 node_enc_dim=1,
                 num_neurons=[128, 128],
                 hidden_activation='ReLU',
                 drop_probability=0.5,
                 normalization=1,
                 out_activation=None):

        super(GraphPredBlock, self).__init__()
        self.node_enc_dim = node_enc_dim
        self.out_activation = out_activation
        self.hidden_activation = hidden_activation
        self.num_neurons = num_neurons
        self.normalization = normalization
        self.drop_probability = drop_probability

        edge_encoder_dict = self._prepare_mlp_params(node_input_dim, node_enc_dim)
        edge_enc = MultiLayerPerceptron(**edge_encoder_dict).to(device)
        self.encoder = torch.nn.ModuleList([edge_enc])

    def forward(self, graphs):
        gs = [g.copy() for g in graphs]
        num_nodes = [len(graph.nodes) for graph in graphs]

        node_attrs = []
        for graph in graphs:
            for n in graph.nodes:
                node_attr = graph.nodes[n]['tensor']
                node_attrs.append(node_attr)

        b_node_enc_input = torch.stack(node_attrs, dim=0)
        b_encoded_node = self.encoder[0](b_node_enc_input)
        b_encoded_node_split = torch.split(b_encoded_node, split_size_or_sections=num_nodes)

        for i, (g, encode_nodes) in enumerate(zip(gs, b_encoded_node_split)):
            for n, node_encoded in zip(graphs[i].nodes, torch.unbind(encode_nodes, dim=0)):
                g.node[n]['tensor'] = node_encoded

        return gs

    def _prepare_mlp_params(self, input_dim, output_dim):
        ret_dict = dict()
        ret_dict['input_dimension'] = input_dim
        ret_dict['output_dimension'] = output_dim
        ret_dict['num_neurons'] = self.num_neurons
        ret_dict['out_activation'] = self.out_activation
        ret_dict['hidden_activation'] = self.hidden_activation
        ret_dict['input_normalization'] = self.normalization
        ret_dict['drop_probability'] = self.drop_probability
        return ret_dict


class Normalizer(torch.nn.Module):

    def __init__(self, num_feature, alpha=0.9):
        super(Normalizer, self).__init__()
        self.alpha = alpha
        self.num_feature = num_feature
        self.register_buffer('mean', torch.zeros(num_feature))
        self.register_buffer('var', torch.zeros(num_feature))
        self.register_buffer('std', torch.zeros(num_feature))
        self.w = Parameter(torch.ones(num_feature))
        self.b = Parameter(torch.zeros(num_feature))
        self.reset_stats()

    def forward(self, xs):
        xs = xs.view(-1, self.num_feature)  # Handling 1-batch case

        if self.training:
            mean_update = torch.mean(xs, dim=0)  # Get mean-values along the batch dimension
            self.mean = self.alpha * self.mean + (1-self.alpha) * mean_update.data
            var_update = (1 - self.alpha) * torch.mean(torch.pow((xs - self.mean), 2), dim=0)
            self.var = self.alpha * self.var + var_update.data
            self.std = torch.sqrt(self.var + 1e-10)

        standardized = xs / self.std
        affined = standardized * torch.nn.functional.relu(self.w)

        return affined

    def reset_stats(self):
        self.mean.zero_()
        self.var.fill_(1)
        self.std.fill_(1)


class PhysicsInducedBias(torch.nn.Module):

    def __init__(self, input_dim=3, use_approx=True, degree=5):
        """
        :param input_dim: (int) input_dim for PhysicsInducedBias layer
        :param use_approx: (bool) If True, exp() will be approximated with power series
        :param degree: (int) degree of power series approximation
        """
        super(PhysicsInducedBias, self).__init__()
        self.input_dim = input_dim
        self.use_approx = use_approx
        self.degree = degree
        self.alpha = Parameter(torch.zeros(1))
        self.r0 = Parameter(torch.zeros(1))
        self.k = Parameter(torch.zeros(1))

        self.alpha.data.fill_(1.0)
        self.r0.data.fill_(1.0)
        self.k.data.fill_(1.0)

        self.norm = Normalizer(self.input_dim)

    def forward(self, xs, degree=None):
        if degree is None:
            degree = self.degree
        interacting_coeiff = self.get_scaled_bias(xs, degree)
        interacting_coeiff = torch.nn.functional.relu(interacting_coeiff)
        return interacting_coeiff

    @staticmethod
    def power_approx(fx, degree=5):
        ret = torch.ones_like(fx)
        fact = 1
        for i in range(1, degree+1):
            fact = fact * i
            ret += torch.pow(fx, i) / fact
        return ret

    def get_scaled_bias(self, xs, degree=5):
        xs = self.norm(xs)
        eps = 1e-10
        inp = torch.split(xs, 1, dim=1)
        x, r, ws = inp[0], inp[1], inp[2]

        r0 = torch.nn.functional.relu(self.r0 + eps)
        alpha = torch.nn.functional.relu(self.alpha + eps)
        k = torch.nn.functional.relu(self.k + eps)

        denom = r0 + k * x
        down_stream_effect = alpha * torch.pow((r0/denom), 2)
        radial_input = -torch.pow((r/denom), 2)

        if self.use_approx:
            radial_effect = self.power_approx(radial_input, degree=degree)
        else:
            radial_effect = torch.exp(-torch.pow((r / denom), 2))

        interacting_coeiff = down_stream_effect * radial_effect

        return interacting_coeiff
