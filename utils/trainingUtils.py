import time
import json
import requests

webhook_url = 'https://hooks.slack.com/services/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'


class Timer:
    def __init__(self, name=None):
        if name is None:
            self.name = 'Operation'
        else:
            self.name = name

    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.interval = self.end - self.start
        print('{} : {} sec'.format(self.name, self.interval))


def slack_mesage(msg):
    slack_data = {'text': "{}".format(msg)}
    response = requests.post(url=webhook_url,
                             data=json.dumps(slack_data),
                             headers={'Content-Type': 'application/json'})

    if response.status_code != 200:
        raise ValueError(
            'Request to slack returned an error {}, the response is:{}'.format(response.status_code, response.text))


def num_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)
