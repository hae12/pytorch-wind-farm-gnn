import numpy as np


def transform(reference_point, wind_dir, target_points):
    # wind_dir (degree)
    # target_point = list of 2d tuples

    if wind_dir < 0:
        wind_dir = wind_dir - abs(wind_dir) % 360
    wind_dir = wind_dir % 360  # 0 <= normalized_wd <= 360

    ref_x, ref_y = reference_point[0], reference_point[1]

    def trsl(ref_point, targets):
        """
            ref_point
            targets (n x 2) numpy array
        """
        targets = np.array(targets)

        return targets - np.array(ref_point)

    wd_rad = np.radians(wind_dir)
    roation_mat = np.array(([np.cos(wd_rad), -np.sin(wd_rad)], [np.sin(wd_rad), np.cos(wd_rad)]))

    trsled = trsl((ref_x, ref_y), target_points)
    return np.stack([np.matmul(roation_mat, p) for p in trsled])


def is_inner(target_point, inner_angle, round_up_decimal=10):
    # assume standardized target_points
    x, y = target_point[0], target_point[1]

    if inner_angle < 0 or inner_angle > 90:
        raise RuntimeError("Set inner angle to be [0, 90]")

    inner_angle_rad = np.radians(inner_angle)
    a = round(np.tan(inner_angle_rad), round_up_decimal)

    below_cond = lambda x, y: True if a * x >= y else False
    above_cond = lambda x, y: True if - a * x <= y else False
    x_pos_cond = True if x > 0 else False

    return below_cond(x, y) and above_cond(x, y) and x_pos_cond


class NodeHelper:
    num_turbines = 0
    c2h = dict()  # coord-to-helper object dictionary
    h2c = dict()  # helper object-to-coord dictionary

    def __init__(self, coord_x, coord_y, influence_angle_th=None, influence_dist_th=None):
        """
        :param coord_x: (float) x-axis position of wind turbine in 2d cartesian coordinate.
        :param coord_y: (float) y-axis position of wind turbine in 2d cartesian coordinate.
        :param influence_angle_th: (float) influence_angle_threshold in degree
        """

        self.index = NodeHelper.num_turbines
        self.built = False
        self.x = coord_x
        self.y = coord_y

        if influence_angle_th is None:
            influence_angle_th = 60
        self.influence_angle_th = influence_angle_th

        if influence_dist_th is None:
            influence_dist_th = np.inf
        self.influence_dist_th = influence_dist_th

        self.interactions = {}

        # Updates for class variables
        NodeHelper.num_turbines += 1
        NodeHelper.c2h[(coord_x, coord_y)] = self
        NodeHelper.h2c[self] = (coord_x, coord_y)

    @classmethod
    def get_helper(cls, coord_x, coord_y, influence_angle_th=None, influence_dist_th=None):
        c2h = cls.c2h
        if (coord_x, coord_y) in c2h:
            helper = c2h[(coord_x, coord_y)]
        else:
            helper = cls(coord_x, coord_y, influence_angle_th, influence_dist_th)
        return helper

    def add_interactions(self, wind_direction, coords, verbose=False):
        transforemd_coords = transform(reference_point=(self.x, self.y),
                                       wind_dir=wind_direction,
                                       target_points=coords)

        for (x, y), (tf_x, tf_y) in zip(coords, transforemd_coords):
            if is_inner(target_point=(tf_x, tf_y), inner_angle=self.influence_angle_th):
                if tf_x == 0 and tf_y == 0:
                    continue  # Ignore itself

                if tf_x == 0:
                    contained_angle = 90
                else:
                    contained_angle = np.rad2deg(abs(np.arctan(tf_y/tf_x)))
                dist = np.linalg.norm((tf_x, tf_y))

                if dist >= self.influence_dist_th:
                    continue  # Ignore too far turbines

                # Note that coordination must be done in original coordinate
                node_helper = self.get_helper(x, y, self.influence_angle_th)
                if verbose:
                    print("influenced turbine index : {}".format(node_helper.index))
                    print("influencing turbine (in original coordination): {}, {}".format(x, y))
                    print("influencing turbine : {}, {} \n".format(tf_x, tf_y))

                self.interactions[node_helper] = {'containedAngle': contained_angle,
                                                  'distance': dist,
                                                  'x': abs(tf_x),
                                                  'r': abs(tf_y)}
        self.built = True

    def clear_interaction(self):
        self.interactions = {}

    @classmethod
    def initialize_node_helper(cls):
        cls.num_turbines = 0
        cls.c2h = dict()
        cls.h2c = dict()