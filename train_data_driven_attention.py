import os
import argparse
import json
import torch
import numpy as np
import sys
from paramiko import SSHClient
from scp import SCPClient
from utils.windFarms import RandomSampleManager
from utils.networks import GraphNeuralNet as GNN
from utils.trainingUtils import Timer, slack_mesage

import warnings
warnings.filterwarnings("error")


# local sever information for downloading saved files
host = '143.248.82.150'
port = 20000
username = 'jyp'
password = 'sil0415'
remote_path = '/home/jyp/gnn_saves'


def put_file(file_paths):
    # instantiate a SSH client for downloading saved files.
    ssh = SSHClient()
    ssh.load_system_host_keys()
    ssh.connect(host, port=port, username=username, password=password)
    with SCPClient(ssh.get_transport()) as scp:
        for file_path in file_paths:
            scp.put(file_path, remote_path=remote_path)
    ssh.close()


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-en", "-experiment-name", help="Name of experiment", type=str)
    args = parser.parse_args()

    if not len(sys.argv) > 1:
        raise RuntimeError("Please specify the name of experiment")

    x_grid_size = 2000
    y_grid_size = 2000
    base_config_path = './configs/example_input_JK100.json'
    update_config_path = './configs/update_floris_configs.json'
    single_input_path = './configs/example_input_single.json'
    min_distance_factor = 2.0
    angle_threshold = 90
    dist_cutoff_factor = 25

    use_speed = True

    edge_input_dim = 2
    if use_speed:
        node_input_dim = 2
    else:
        node_input_dim = 1
    global_input_dim = 2

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device = "cpu"
    name = args.en

    gnn = GNN(name=name,
              edge_input_dim=edge_input_dim,
              node_input_dim=node_input_dim,
              global_input_dim=global_input_dim,
              node_enc_dim=50,
              edge_enc_dim=50,
              global_enc_dim=50,
              output_activation='ReLU',
              num_neurons=[256, 256, 128],
              num_gn_block=3,
              normalization=[1, 2, 2, 2],
              attention=[False, False, True],
              device=device,
              residual=True,
              hidden_activation='LeakyReLU',
              gn_output_activation='LeakyReLU',
              attention_out_activation='ReLU',
              physics_induced_bias=[False, False, False],
              drop_probability=0.0)

    num_turbines = [5, 10, 15, 20]
    num_samples = len(num_turbines)
    min_wind_dir = 0
    max_wind_dir = 360
    num_wd_sampling = 15  # 10

    min_wind_speed = 6.0
    max_wind_speed = 15.0
    num_ws_sampling = 15  # 10

    train_steps = 200
    val_steps = 1
    init_temp = 1.0

    noise_factor = 0.00

    # download to the local server
    save_local = True

    save_path = os.path.join('./model_saves', '{}_config'.format(name))
    file_paths = [save_path + '.json']

    grid_sizes = [2000, 2000, 2000, 2000]
    wind_farms = []
    for grid_size in grid_sizes:
        wind_farm = RandomSampleManager(x_grid_size=grid_size,
                                        y_grid_size=grid_size,
                                        update_config_json_path=update_config_path,
                                        base_config_json_path=base_config_path,
                                        single_input_path=single_input_path,
                                        angle_threshold=angle_threshold,
                                        min_distance_factor=min_distance_factor,
                                        dist_cutoff_factor=25)
        wind_farms.append(wind_farm)

    use_power = True

    xs = []
    ys = []

    xs_val = []
    ys_val = []

    for nt, wind_farm in zip(num_turbines, wind_farms):
        with Timer('train_data_sampling'):
            for i in range(num_wd_sampling * num_ws_sampling):
                wind_farm.sample_wind_farm(nt)
                wd = np.random.uniform(low=min_wind_dir, high=max_wind_dir, size=1)
                ws = np.random.uniform(low=min_wind_speed, high=max_wind_speed, size=1)
                try:
                    wind_farm.update_wind_farm_graph(wd[0], ws[0], verbose=False)
                    x, y = wind_farm.observe(use_power, noise_factor=noise_factor)
                    xs.append(x)
                    ys.append(y)
                except RuntimeWarning:
                    continue

        with Timer('validation_data_sampling'):
            for j in range(num_wd_sampling * num_ws_sampling):
                wind_farm.sample_wind_farm(nt)
                wd = np.random.uniform(low=min_wind_dir, high=max_wind_dir, size=1)
                ws = np.random.uniform(low=min_wind_speed, high=max_wind_speed, size=1)
                try:
                    wind_farm.update_wind_farm_graph(wd[0], ws[0], verbose=False)
                    x_v, y_v = wind_farm.observe(use_power, noise_factor=noise_factor)
                    xs_val.append(x_v)
                    ys_val.append(y_v)
                except RuntimeWarning:
                    continue

    print('wind-farm layout : {} samples'.format(len(xs)))
    with Timer('Training loop'):
        hist = gnn.fit(xs, ys,
                       xs_val, ys_val,
                       train_steps=train_steps,
                       val_steps=val_steps,
                       use_mse=True)

    print(hist)

    # Save model
    save_path = os.path.join('./model_saves', '{}'.format(gnn.name))
    gnn.save(save_path)

    # Save training history
    hist_save_path = os.path.join('./model_saves', '{}_hist.json'.format(gnn.name))
    with open(hist_save_path, 'w') as f:
        json.dump(hist, f)

    try:
        # Transfer learned model to the local server
        if save_local:
            file_paths = [save_path+'.json', save_path]
            put_file(file_paths)

        if save_local:
            file_paths = [hist_save_path]
            put_file(file_paths)
    except:
        raise RuntimeError("Network error :(")

    finally:
        pass
